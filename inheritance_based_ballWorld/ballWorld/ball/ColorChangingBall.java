package ballWorld.ball;

import java.awt.Color;
import java.awt.Component;
import java.awt.Point;

import util.Randomizer;

/**
 * This class implements a straight line moving behavior with uniform velocity.
 * Additionally, it varies the ball color randomly.
 * The position and velocity is corrected on collisions with canvas walls.
 * @author Shoeb Mohammed
 *
 */
public class ColorChangingBall extends StraightBall {

	public ColorChangingBall(Point startLoc, Integer startRadius, Point startVel, Color startColor,
			Integer startTimeSlice, Component startCanvas) {
		super(startLoc, startRadius, startVel, startColor, startTimeSlice, startCanvas);
	}

	@Override
	public void moveDt(Double dt) {
		super.moveDt(dt);
		setColor(Randomizer.Singleton.randomColor());
	}
}
