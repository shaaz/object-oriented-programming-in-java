package ballWorld.ball;

import java.awt.Color;
import java.awt.Component;
import java.awt.Point;

import util.Randomizer;

/**
 * This class implements a straight line moving behavior with uniform velocity.
 * Additionally, it varies the radius like a heart beat with some beat rate.
 * The position and velocity is corrected on collisions with canvas walls.
 * @author Shoeb Mohammed
 *
 */
public class HeartBeatBall extends StraightBall {

	private Double _beatVelocity;
	private Double _beatRadius;

	public HeartBeatBall(Point startLoc, Integer startRadius, Point startVel, Color startColor, Integer startTimeSlice,
			Component startCanvas) {
		super(startLoc, startRadius, startVel, startColor, startTimeSlice, startCanvas);
		_beatVelocity = Randomizer.Singleton.randomDouble(1.0, 30.0);
		_beatRadius = (double) startRadius;
	}

	@Override
	public void moveDt(Double dt) {
		super.moveDt(dt);
		Double radius = getRadius();
		radius += _beatVelocity * dt / 1000.0;
		setRadius(radius);
	}

	@Override
	public void correct4collision() {
		super.correct4collision();
		Double q = Math.floor(getRadius() / _beatRadius);
		Double rem = getRadius() - q * _beatRadius;
		setRadius(rem + 1.0);
	}
}
