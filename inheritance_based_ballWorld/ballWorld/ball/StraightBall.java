package ballWorld.ball;

import java.awt.Color;
import java.awt.Component;
import java.awt.Point;
import java.awt.geom.Point2D;

/**
 * This class implements a straight line moving behavior with uniform velocity.
 * The position and velocity is corrected on collisions with canvas walls.
 * @author Shoeb Mohammed
 *
 */
public class StraightBall extends ABall {

	public StraightBall(Point startLoc, Integer startRadius, Point startVel, Color startColor, Integer startTimeSlice,
			Component startCanvas) {
		Point2D.Double sLoc = new Point2D.Double(startLoc.x, startLoc.y);
		Point2D.Double sVel = new Point2D.Double(startVel.x, startVel.y);
		super.init(sLoc, (double) startRadius, sVel, startColor, (double) startTimeSlice, startCanvas);
	}

	@Override
	public void moveDt(Double dt) {
		Double newX = getLoc().x + (getVelocity().x * dt / 1000.0);
		Double newY = getLoc().y + (getVelocity().y * dt / 1000.0);
		setLoc(new Point2D.Double(newX, newY));
	}

	@Override
	public void correct4collision() {
		if (validLoc(getLoc()))
			return;
		setLoc(getNewLoc());
		setVelocity(getNewVel());
		//		correct4collision();
	}

	/**
	 * This function returns the velocity after first collision.
	 * That is if lastLocation and currentLocation of the ball are
	 * such that there is collision then it returns the new position
	 * after collision. 
	 * @return Point2D.Double velocity after first collision.
	 */
	protected Point2D.Double getNewVel() {
		Point2D.Double lastLoc = getLastLoc();
		Double x1 = lastLoc.x;
		Double x2 = getLoc().x;
		Double y1 = lastLoc.y;
		Double y2 = getLoc().y;
		Double width = (double) super._canvas.getWidth();
		Double height = (double) super._canvas.getHeight();
		Point2D.Double newVel = new Point2D.Double(getVelocity().x, getVelocity().y);

		if (x1 == x2)
			newVel.y *= -1;
		else if (y1 == y2)
			newVel.x *= -1;
		else {
			Double _lamLeft = Math.abs(x1 / (x1 - x2));
			Double _lamRight = Math.abs((x1 - width) / (x1 - x2));
			Double _lamTop = Math.abs(y1 / (y1 - y2));
			Double _lamBottom = Math.abs((y1 - height) / (y1 - y2));

			if ((x1 / (x1 - x2) == Math.min(Math.min(_lamLeft, _lamTop), _lamBottom))
					|| ((x1 - width) / (x1 - x2) == Math.min(Math.min(_lamRight, _lamTop), _lamBottom))) //collision on left or right wall
				newVel.x *= -1;
			if ((y1 / (y1 - y2) == Math.min(Math.min(_lamTop, _lamLeft), _lamRight))
					|| ((y1 - height) / (y1 - y2) == Math.min(Math.min(_lamBottom, _lamLeft), _lamRight))) //collision on top wall or bottom wall		
				newVel.y *= -1;
		}
		return newVel;
	}

	/**
	 * This function returns the position after first collision.
	 * That is if lastLocation and currentLocation of the ball are
	 * such that there is collision then it returns the new position
	 * after collision. Note the new position may not be within
	 * the confines of the canvas.
	 * @return Point2D.Double position after first collision.
	 */
	protected Point2D.Double getNewLoc() {
		Point2D.Double lastLoc = getLastLoc();
		Double x1 = lastLoc.x;
		Double x2 = getLoc().x;
		Double y1 = lastLoc.y;
		Double y2 = getLoc().y;
		Double width = (double) super._canvas.getWidth();
		Double height = (double) super._canvas.getHeight();
		Point2D.Double contactLoc = getContactLoc();
		Point2D.Double newLoc = new Point2D.Double(x2, y2);
		if (x1 == x2)
			newLoc.y = (2 * contactLoc.y - y2);
		else if (y1 == y2)
			newLoc.x = (2 * contactLoc.x - x2);

		else {
			Double _lamLeft = Math.abs(x1 / (x1 - x2));
			Double _lamRight = Math.abs((x1 - width) / (x1 - x2));
			Double _lamTop = Math.abs(y1 / (y1 - y2));
			Double _lamBottom = Math.abs((y1 - height) / (y1 - y2));

			if ((x1 / (x1 - x2) == Math.min(Math.min(_lamLeft, _lamTop), _lamBottom))
					|| ((x1 - width) / (x1 - x2) == Math.min(Math.min(_lamRight, _lamTop), _lamBottom))) //collision on left or right wall
				newLoc.x = (2 * contactLoc.x - x2);
			if ((y1 / (y1 - y2) == Math.min(Math.min(_lamTop, _lamLeft), _lamRight))
					|| ((y1 - height) / (y1 - y2) == Math.min(Math.min(_lamBottom, _lamLeft), _lamRight))) //collision on top wall or bottom wall
				newLoc.y = (2 * contactLoc.y - y2);
		}
		return newLoc;
	}

	/**
	 * This function returns the location of the ball center at moment of 
	 * contact at first collision.
	 * That is if lastLocation and currentLocation of the ball are
	 * such that there is collision then it returns the contact position
	 * at moment of collision. Note that the contact position must be within
	 * the confines of the canvas.
	 * @return Point2D.Double position at moment of collision.
	 */
	protected Point2D.Double getContactLoc() {
		Point2D.Double lastLoc = getLastLoc();
		Double x1 = lastLoc.x;
		Double x2 = getLoc().x;
		Double y1 = lastLoc.y;
		Double y2 = getLoc().y;
		Double r = getRadius();
		Double width = (double) super._canvas.getWidth();
		Double height = (double) super._canvas.getHeight();
		Double lambda = 0.0;

		if (x1 == x2) {
			if (y1 == y2)
				lambda = 0.0;
			else if (y1 / (y1 - y2) >= 0)
				lambda = (r - y1) / (y2 - y1);
			else
				lambda = (height - r - y1) / (y2 - y1);
		} else if (y1 == y2) {
			if (x1 == x2)
				lambda = 0.0;
			else if (x1 / (x1 - x2) >= 0)
				lambda = (r - x1) / (x2 - x1);
			else
				lambda = (width - r - x1) / (x2 - x1);
		} else {
			Double _lamLeft = Math.abs(x1 / (x1 - x2));
			Double _lamRight = Math.abs((x1 - width) / (x1 - x2));
			Double _lamTop = Math.abs(y1 / (y1 - y2));
			Double _lamBottom = Math.abs((y1 - height) / (y1 - y2));

			if (x1 / (x1 - x2) == Math.min(Math.min(_lamLeft, _lamTop), _lamBottom)) //collision on left wall
				lambda = (r - x1) / (x2 - x1);
			if ((x1 - width) / (x1 - x2) == Math.min(Math.min(_lamRight, _lamTop), _lamBottom)) //collision on right wall
				lambda = (width - r - x1) / (x2 - x1);
			if (y1 / (y1 - y2) == Math.min(Math.min(_lamTop, _lamLeft), _lamRight)) //collision on top wall
				lambda = (r - y1) / (y2 - y1);
			if ((y1 - height) / (y1 - y2) == Math.min(Math.min(_lamBottom, _lamLeft), _lamRight)) //collision on bottom wall 
				lambda = (height - r - y1) / (y2 - y1);
		}
		return new Point2D.Double(x1 + lambda * (x2 - x1), y1 + lambda * (y2 - y1));
	}

	/**
	 * This function returns true if the user supplied argument is
	 * within the confines of the canvas.
	 * @param loc Pont2D.Double
	 * @return boolean
	 */
	private boolean validLoc(Point2D.Double loc) {
		Double width = (double) super._canvas.getWidth();
		Double height = (double) super._canvas.getHeight();
		Double radius = getRadius();
		if (loc.x >= radius && loc.x <= (width - radius) && loc.y >= radius && loc.y <= (height - radius))
			return true;
		else
			return false;
	}

}
