package ballWorld.ball;

import java.awt.Color;
import java.awt.Component;
import java.awt.Point;

import util.Randomizer;

/**
 * This class implements a straight line moving behavior with uniform velocity.
 * Additionally, it varies the radius like a beatBreathe with some beat rate.
 * The position and velocity is corrected on collisions with canvas walls.
 * @author Shoeb Mohammed
 *
 */
public class BreathingBall extends StraightBall {
	private Double _breathVelocity;
	private Double _breathRadius;
	private int _breathDir;

	public BreathingBall(Point startLoc, Integer startRadius, Point startVel, Color startColor, Integer startTimeSlice,
			Component startCanvas) {
		super(startLoc, startRadius, startVel, startColor, startTimeSlice, startCanvas);
		_breathVelocity = Randomizer.Singleton.randomDouble(1.0, 30.0);
		_breathRadius = (double) startRadius;
		_breathDir = -1;
	}

	@Override
	public void moveDt(Double dt) {
		super.moveDt(dt);
		Double radius = getRadius() + _breathDir * _breathVelocity * dt / 1000;
		setRadius(radius);
	}

	@Override
	public void correct4collision() {
		super.correct4collision();
		if (_breathDir == -1 && getRadius() <= 0) {
			_breathDir = 1;
			setRadius(-1.0 * getRadius());
		}
		if (_breathDir == 1 && getRadius() >= _breathRadius) {
			_breathDir = -1;
			setRadius(2 * _breathRadius - getRadius());
		}
	}
}
