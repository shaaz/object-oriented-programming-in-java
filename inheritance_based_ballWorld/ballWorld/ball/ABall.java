package ballWorld.ball;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.geom.Point2D;
import java.util.Observable;
import java.util.Observer;

import shape.AShape;

/**
 *  This is an abstract class representing a moving ball within the canvas.
 *  Nothing is assumed about how the ball moves or behaves while moving.
 *  The particular moving/behaviors will be implemented by concrete sub-classes.
 * @author Shoeb Mohammed
 *
 */
public abstract class ABall extends AShape implements Observer {
	public void update(Observable o, Object g) {
		moveNextTimeSlice();
		paint((Graphics) g);
	}

	public void moveNextTimeSlice() {
		//Point2D.Double lastLoc = getLoc();
		moveDt(_timeSlice);
		correct4collision();
	}

	/**
	 * Move the ball by specified time.
	 * @param timeSlice a double time value in milliseconds.
	 */
	public abstract void moveDt(Double dt);

	/**
	 * Correct for any collisions with the walls of the canvas.
	 * Invoking this function makes sense only after moving the ball.
	 * Otherwise it just returns.
	 */
	public abstract void correct4collision();

	private Point2D.Double _loc;
	private Point2D.Double _lastLoc;
	private Double _radius;
	private Point2D.Double _vel;
	private Color _color;
	protected Component _canvas;
	protected Double _timeSlice;

	protected void setLoc(Point2D.Double loc) {
		setLastLoc(getLoc());
		_loc = loc;
		super.setCenterMassX((int) Math.round(loc.x));
		super.setCenterMassY((int) Math.round(loc.x));
	}

	public Point2D.Double getLoc() {
		return _loc;
	}

	protected void setLastLoc(Point2D.Double loc) {
		_lastLoc = loc;
	}

	public Point2D.Double getLastLoc() {
		return _lastLoc;
	}

	protected void setRadius(Double radius) {
		_radius = radius;
	}

	public Double getRadius() {
		return _radius;
	}

	protected void setVelocity(Point2D.Double vel) {
		_vel = vel;
	}

	public Point2D.Double getVelocity() {
		return _vel;
	}

	protected void setColor(Color color) {
		_color = color;
	}

	public Color getColor() {
		return _color;
	}

	public Double getTimeSlice() {
		return _timeSlice;
	}

	public void paint(Graphics g) {
		g.setColor(_color);
		int locX = (int) Math.round(getLoc().x);
		int locY = (int) Math.round(getLoc().y);
		int radius = (int) Math.round(getRadius());
		g.fillOval(locX - radius, locY - radius, 2 * radius, 2 * radius);
	}

	@Override
	public void paint(Graphics g, int x, int y) {
		g.setColor(_color);
		int radius = (int) Math.round(getRadius());
		g.fillOval(x - radius, y - radius, 2 * radius, 2 * radius);
	}

	protected void init(Point2D.Double startLoc, Double startRadius, Point2D.Double startVel, Color startColor,
			Double startTimeSlice, Component startCanvas) {
		_loc = startLoc;
		_lastLoc = startLoc;
		_radius = startRadius;
		_vel = startVel;
		_color = startColor;
		_timeSlice = startTimeSlice;
		_canvas = startCanvas;
		super.setCenterMassX((int) Math.round(startLoc.x));
		super.setCenterMassY((int) Math.round(startLoc.x));
	}
}
