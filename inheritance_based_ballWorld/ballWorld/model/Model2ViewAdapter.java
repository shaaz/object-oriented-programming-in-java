package ballWorld.model;

import java.awt.Dimension;

import javax.swing.JPanel;

import ballWorld.view.*;

public class Model2ViewAdapter implements IModel2ViewAdapter {

	private BallGUI _view;

	public Model2ViewAdapter(BallGUI gui) {
		setView(gui);
	}

	@Override
	public void update() {
		_view.update();
	}

	public BallGUI getView() {
		return _view;
	}

	public void setView(BallGUI view) {
		this._view = view;
	}

	@Override
	public Dimension getViewSize() {
		return _view.getCanvasSize();
	}

	@Override
	public JPanel getViewCanvas() {
		return _view.getCanvas();
	}

}
