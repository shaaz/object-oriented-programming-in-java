package ballWorld.model;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Timer;

import ballWorld.ball.ABall;
import util.Dispatcher;
import util.Randomizer;

/**
 * Model of the ball world. Model interacts with the view via a Model2ViewAdapter.
 * This model is used by the controller.
 * @author Shoeb Mohammed (with hints/help from Dr. Stephen Wong)
 *
 */
public class BallModel {
	private IModel2ViewAdapter _model2ViewAdpt = IModel2ViewAdapter.NULL_OBJECT; // Insures that the adapter is always valid
	private int _timeSlice = 33; //update every 33 milliseconds
	private Timer _timer = new Timer(_timeSlice, new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			_model2ViewAdpt.update();
		}
	});
	private Dispatcher myDispatcher = new Dispatcher();

	private void addBall2Dispatcher(ABall ball) {
		myDispatcher.addObserver(ball);
	}

	private void resetDispatcher() {
		myDispatcher.deleteObservers();
	}

	/**
	* The following method returns an instance of an ABall, given a fully qualified class name (package.classname) of
	* a subclass of ABall.
	* The method assumes that there is only one constructor for the supplied class that has the same *number* of
	* input parameters as specified in the args array and that it conforms to a specific
	* signature, i.e. specific order and types of input parameters in the args array.
	* @param className A string that is the fully qualified class name of the desired object
	* @return An instance of the supplied class. 
	*/
	private ABall loadBall(String className) {
		try {
			Object[] args = new Object[] { Randomizer.Singleton.randomLoc(_model2ViewAdpt.getViewSize()),
					Randomizer.Singleton.randomInt(5, 25),
					Randomizer.Singleton.randomVel(new Rectangle(0, 0, 250, 250)), Randomizer.Singleton.randomColor(),
					_timeSlice, _model2ViewAdpt.getViewCanvas() };
			java.lang.reflect.Constructor<?> cs[] = Class.forName(className).getConstructors(); // get all the constructors
			java.lang.reflect.Constructor<?> c = null;
			for (int i = 0; i < cs.length; i++) { // find the first constructor with the right number of input parameters
				if (args.length == (cs[i]).getParameterTypes().length) {
					c = cs[i];
					break;
				}
			}
			return (ABall) c.newInstance(args); // Call the constructor.   Will throw a null ptr exception if no constructor with the right number of input parameters was found.
		} catch (Exception ex) {
			System.err.println("Class " + className + " failed to load. \nException = \n" + ex);
			ex.printStackTrace(); // print the stack trace to help in debugging.
			return null; // Is this really a useful thing to return here?  Is there something better that could be returned? 
		}
	}

	public BallModel(IModel2ViewAdapter model2viewAdpt) {
		_model2ViewAdpt = model2viewAdpt;
	}

	private void startTimer() {
		_timer.start();
	}

	private void stopTimer() {
		_timer.stop();
	}

	/**
	 * This is the method that is called by the view's adapter to the model, i.e. is called by IView2ModelAdapter.paint().
	 * This method will update the sprites's painted locations by painting all the balls.
	 * onto the given Graphics object.
	 * @param g The Graphics object from the view's paintComponent() call.
	 */
	public void update(Graphics g) {
		myDispatcher.notifyAll(g); // The Graphics object is being given to all the balls (Observers)
	}

	public void clearAll(Graphics g) {
		resetDispatcher();
	}

	public void makeBall(String s) {
		addBall2Dispatcher(loadBall(s));
	}

	public void start() {
		startTimer();
	}

	public void stop() {
		stopTimer();
	}
}
