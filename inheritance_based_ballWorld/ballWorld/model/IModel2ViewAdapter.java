package ballWorld.model;

import java.awt.Dimension;

import javax.swing.JPanel;

/**
 * Interface that goes from the model to the view that enables the model to talk to the view
 * @author Shoeb Mohammed (with hints/help from Comp504 Dr. Stephen Wong)
 *
 */
public interface IModel2ViewAdapter {

	/**
	 * The method that tells the view to update
	 */
	public void update();

	/**
	 * get the dimensions of the attached view.
	 * @return Dimension object representing the dimensions of the attached view.
	 */
	public Dimension getViewSize();

	/**
	 * return the canvas (animation area) of the view
	 * @return JPanel
	 */
	public JPanel getViewCanvas();

	/**
	 * No-op "null" adapter
	 */
	public static final IModel2ViewAdapter NULL_OBJECT = new IModel2ViewAdapter() {
		public void update() {
		}

		public Dimension getViewSize() {
			return null;
		}

		public JPanel getViewCanvas() {
			return null;
		}
	};

}
