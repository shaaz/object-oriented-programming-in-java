package ballWorld.controller;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Graphics;

import javax.swing.JPanel;

import ballWorld.model.IModel2ViewAdapter;
import ballWorld.view.IView2ModelAdapter;
import ballWorld.model.BallModel;
import ballWorld.view.BallGUI;

/**
 * Controller for BallModel and BallGUI.
 * @author Shoeb Mohammed
 *
 */
public class BallController {
	private BallModel _model;
	private BallGUI _view;

	BallController() {
		_model = new BallModel(new IModel2ViewAdapter() {
			@Override
			public void update() {
				_view.update();
			}

			@Override
			public Dimension getViewSize() {
				return _view.getCanvasSize();
			}

			@Override
			public JPanel getViewCanvas() {
				return _view.getCanvas();
			}
		});

		_view = new BallGUI(new IView2ModelAdapter() {
			@Override
			public void paint(Graphics g) {
				_model.update(g);
			}

			@Override
			public void clearAll(Graphics g) {
				_model.clearAll(g);
			}

			@Override
			public void make(String s) {
				_model.makeBall(s);
			}
		});
	}

	/**
	 * Start the system
	 */
	public void start() {
		_model.start(); // It is usually better to start the model first but not always.
		_view.start();
	}

	/**
	 * Launch the application.
	 * @param args Arguments given by the system or command line.
	 */

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() { // Java specs say that the system must be constructed on the GUI event thread.
			public void run() {
				try {
					BallController controller = new BallController(); // instantiate the system
					controller.start(); // start the system
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

}
