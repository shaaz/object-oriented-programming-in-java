package ballWorld.view;

import java.awt.Graphics;

/**
 * The interface of the adapter from view to the model.
 * @author Shoeb Mohammed (with hints and help from Comp504, Dr. Stephen Wong).
 *
 */
public interface IView2ModelAdapter {
	public void paint(Graphics g);

	/**
	 * Tells the model to clear everything (reset).
	 * @param g Graphics object
	 */
	public void clearAll(Graphics g);

	/**
	 * Tells the model to dynamically load a class.
	 * @param s String which is a fully qualified name of the class.
	 */
	public void make(String s);

	/**
	 * No-op null adapter
	 */
	public static final IView2ModelAdapter NULL_OBJECT = new IView2ModelAdapter() {
		public void paint(Graphics g) {
		}

		public void clearAll(Graphics g) {
		}

		public void make(String s) {
		}
	};

}
