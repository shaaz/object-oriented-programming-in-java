package ballWorld.view;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Graphics;
import java.awt.Color;

/**
 * This class encapsulates the view (GUI) for the ballWorld.
 * @author Shoeb Mohammed
 *
 */
public class BallGUI extends JFrame {

	private static final long serialVersionUID = 296259226903282209L;
	private JPanel contentPane;
	private final JPanel pnlControl = new JPanel();
	private final JTextField txtInput = new JTextField();
	private final JButton btnMakeBall = new JButton("Make Ball");
	private final JButton btnClearAll = new JButton("Clear All");
	
    // The model adapter is initialized to a no-op to insure that system always has well-defined behavior
    private IView2ModelAdapter _view2ModelAdpt = IView2ModelAdapter.NULL_OBJECT;  

    // Create a special panel with an overridden paintComponent method.	
    private final JPanel pnlCanvas = new JPanel(){
		private static final long serialVersionUID = 5506003142825745419L;

		public void paintComponent(Graphics g){
			super.paintComponent(g); // clear the panel and redo the background
			_view2ModelAdpt.paint(g); // call back to the model to paint the balls
		}
	};


	/**
	 * Create the frame with null Adapter.
	 */
	public BallGUI() {
		txtInput.setToolTipText("Input the fully qualified name of the ball.");
		txtInput.setText("ballWorld.ball.StraightBall");
		txtInput.setColumns(10);
		initGUI();
	}
	
	/**
	 * Create the frame with user provided adapter.
	 * @param adpt Adapter that implements IView2ModelAdapter interface.
	 */
	public BallGUI(IView2ModelAdapter adpt){
		txtInput.setToolTipText("Input the fully qualified name of the ball.");
		txtInput.setText("ballWorld.ball.StraightBall");
		txtInput.setColumns(10);
		_view2ModelAdpt = adpt;
		initGUI();
	}
	
	/**
	 * start the frame
	 */
	public void start() {
		setVisible(true);
	}
	
	/**
	 * initialize the ballGUI.
	 */
	private void initGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 553, 442);
		
		contentPane = new JPanel();
		
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		pnlControl.setBackground(Color.LIGHT_GRAY);
		pnlControl.setToolTipText("Control Panel.");
		
		contentPane.add(pnlControl, BorderLayout.NORTH);
		
		pnlControl.add(txtInput);
		btnMakeBall.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				_view2ModelAdpt.make(txtInput.getText());
			}
		});
		
		btnMakeBall.setToolTipText("Click to make input ball.");
		
		pnlControl.add(btnMakeBall);
		btnClearAll.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Dimension pnlCanvasDim = pnlCanvas.getSize();
				pnlCanvas.getGraphics().clearRect(0, 0, pnlCanvasDim.width, pnlCanvasDim.height);
				_view2ModelAdpt.clearAll(contentPane.getGraphics());
			}
		});
		btnClearAll.setToolTipText("Clear the screen(animation).");
		
		pnlControl.add(btnClearAll);
		pnlCanvas.setToolTipText("Animations are played here.");
		
		contentPane.add(pnlCanvas, BorderLayout.CENTER);
	}
	
	public void update(){
		pnlCanvas.repaint();
	}

	public Dimension getCanvasSize() {
		return pnlCanvas.getSize();
	}

	public JPanel getCanvas() {
		return pnlCanvas;
	}

}
