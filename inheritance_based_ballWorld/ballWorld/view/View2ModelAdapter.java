package ballWorld.view;

import java.awt.Graphics;

import ballWorld.model.*;

/**
 * Class implementing View to Model adapter interface.
 * @author Shoeb Mohammed
 *
 */
public class View2ModelAdapter implements IView2ModelAdapter {

	private BallModel _model;

	public View2ModelAdapter(BallModel model) {
		setModel(model);
	}

	public BallModel getModel() {
		return _model;
	}

	public void setModel(BallModel model) {
		this._model = model;
	}

	@Override
	public void paint(Graphics g) {
		_model.update(g);
	}

	@Override
	public void clearAll(Graphics g) {
		_model.clearAll(g);
	}

	@Override
	public void make(String s) {
		_model.makeBall(s);
	}

}
