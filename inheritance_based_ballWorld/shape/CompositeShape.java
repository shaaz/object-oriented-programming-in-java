package shape;

import shape.AShape;
import java.awt.Graphics;

/**
 * A composite shape uses Composite Design pattern to draw complex shapes.
 * 
 */
public class CompositeShape extends AShape {
	private AShape[] shapes;

	public void setShapes(AShape[] value) {
		this.shapes = value;
	}

	public AShape[] getShapes() {
		return this.shapes;
	}

	public void paint(Graphics g, int x, int y) {
		int h = super.getCenterMassX() - x;
		int k = super.getCenterMassY() - y;

		for (int i = 0; i < shapes.length; ++i)
			shapes[i].paint(g, shapes[i].getCenterMassX() - h, shapes[i].getCenterMassY() - k);
	}

	public CompositeShape(AShape[] shapes) {
		this.shapes = shapes;
		int cX = 0;
		int cY = 0;
		for (int i = 0; i < shapes.length; ++i) {
			cX += shapes[i].getCenterMassX();
			cY += shapes[i].getCenterMassY();
		}
		super.setCenterMassX(cX / shapes.length);
		super.setCenterMassY(cY / shapes.length);
	}

}
