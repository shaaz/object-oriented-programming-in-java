package shape;

import java.awt.Graphics;

/**
 * Abstract shape class has no fields.
 * A shape only knows how to draw itself.  */
public abstract class AShape {
	/**
	 * paint shape at given location.
	 * @param g Graphics object to paint on.
	 * @param x x-coordinate of center of mass/centroid.
	 * @param y y-coordinate of center of mass/centroid.
	 */
	private int centerMassX;

	public void setCenterMassX(int value) {
		this.centerMassX = value;
	}

	public int getCenterMassX() {
		return this.centerMassX;
	}

	private int centerMassY;

	public void setCenterMassY(int value) {
		this.centerMassY = value;
	}

	public int getCenterMassY() {
		return this.centerMassY;
	}

	public abstract void paint(Graphics g, int x, int y);
}