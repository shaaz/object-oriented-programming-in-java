package shape;

import java.awt.Graphics;

/**
 * Rectangle is a shape with center of mass at origin.
 */
public class Rectangle extends AShape {
	public Rectangle(int w, int h) {
		super.setCenterMassX(w / 2);
		super.setCenterMassY(h / 2);
		width = w;
		height = h;
	}

	private int width;

	public void setWidth(int value) {
		this.width = value;
	}

	public int getWidth() {
		return this.width;
	}

	private int height;

	public void setHeight(int value) {
		this.height = value;
	}

	public int getHeight() {
		return this.height;
	}

	public void paint(Graphics g, int x, int y) {
		g.fillRect(x, y + height, width, height);
	}

}
