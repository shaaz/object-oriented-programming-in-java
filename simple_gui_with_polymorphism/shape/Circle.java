package shape;

import java.awt.Graphics;

/**
 * A circle a shape with radius r and center at origin. */
public class Circle extends AShape {
	public Circle(int r) {
		radius = r;
		super.setCenterMassX(0);
		super.setCenterMassY(0);
	}

	private int radius;

	public void setRadius(int value) {
		this.radius = value;
	}

	public int getRadius() {
		return this.radius;
	}

	public void paint(Graphics g, int x, int y) {
		g.fillOval(x - radius, y + radius, 2 * radius, 2 * radius);
	}

}
