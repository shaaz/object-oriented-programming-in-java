package shape;

import java.awt.Graphics;

/**
 * A Triangle is shape specified by its three points.
 *  */
public class Triangle extends AShape {
	public Triangle(int[] xPoints, int[] yPoints) {
		this.xCoord = xPoints;
		this.yCoord = yPoints;
		super.setCenterMassX((xPoints[0] + xPoints[1] + xPoints[2]) / 3);
		super.setCenterMassY((yPoints[0] + yPoints[1] + yPoints[2]) / 3);
	}

	private int[] xCoord;
	private int[] yCoord;

	public void paint(Graphics g, int x, int y) {
		int h = (xCoord[0] + xCoord[1] + xCoord[2]) / 3 - x;
		int k = (yCoord[0] + yCoord[1] + yCoord[2]) / 3 - y;

		int[] xPoints = { xCoord[0] - h, xCoord[1] - h, xCoord[2] - h };
		int[] yPoints = { yCoord[0] - k, yCoord[1] - k, yCoord[2] - k };
		g.fillPolygon(xPoints, yPoints, 3);
	}

}
