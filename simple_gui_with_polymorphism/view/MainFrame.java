package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.SystemColor;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;

import shape.AShape;
import shape.Rectangle;
import shape.Triangle;
import shape.Circle;
import shape.CompositeShape;

public class MainFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3256914973903308813L;
	private JPanel contentPane;
	private AShape shape;

	private final JPanel pnlCenter = new JPanel() {
		/**
		 * 
		 */
		private static final long serialVersionUID = -872444218515942499L;

		/**
		 * Overridden paintComponent method to paint a shape in the panel.
		 * @param g The Graphics object to paint on.
		 */
		public void paintComponent(Graphics g) {
			super.paintComponent(g);
			g.setColor(Color.RED);
			g.fillOval(75, 100, 20, 40);
			shape.paint(g, 175, 50);
		}
	};
	private final JPanel pnlNorth = new JPanel();
	private final JLabel lblInfo = new JLabel("Output");
	private final JButton btnRun = new JButton("Run");
	private final JTextField txtInput = new JTextField();
	private final JPanel pnlSouth = new JPanel();
	private final JButton btnTriangle = new JButton("Triangle");
	private final JButton btnCircle = new JButton("Circle");
	private final JButton btnCompositeShape = new JButton("Composite Shape");

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainFrame frame = new MainFrame();
					frame.start();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * start the frame
	 */
	public void start() {
		setVisible(true);
	}

	/**
	 * Create the frame.
	 */
	public MainFrame() {
		txtInput.setToolTipText("Enter your message here and click Run button.");
		txtInput.setText("Input Message");
		txtInput.setColumns(10);
		initGUI();
	}

	private void initGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		pnlCenter.setToolTipText("canvas area");
		pnlCenter.setBackground(SystemColor.textHighlight);

		contentPane.add(pnlCenter, BorderLayout.CENTER);
		pnlNorth.setToolTipText("control panel");
		pnlNorth.setBackground(Color.ORANGE);

		contentPane.add(pnlNorth, BorderLayout.NORTH);

		pnlNorth.add(txtInput);
		btnRun.setToolTipText("Clicking Run will copy text on left side to right side.");
		btnRun.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblInfo.setText(txtInput.getText());
				//lblInfo.setText(new String("Comp504 rocks!"));
			}
		});

		pnlNorth.add(btnRun);
		lblInfo.setToolTipText("Output display.");

		pnlNorth.add(lblInfo);

		shape = new Rectangle(100, 100);

		pnlSouth.setToolTipText("Choose shapes to draw.");
		pnlSouth.setBackground(Color.PINK);

		contentPane.add(pnlSouth, BorderLayout.SOUTH);
		btnTriangle.setToolTipText("Click to draw a triangle shape.");
		btnTriangle.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int[] xPoints = { 0, 100, 50 };
				int[] yPoints = { 0, 0, 100 };
				shape = new Triangle(xPoints, yPoints);
				pnlCenter.repaint();
			}
		});

		pnlSouth.add(btnTriangle);
		btnCircle.setToolTipText("Click to draw a circle shape.");
		btnCircle.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				shape = new Circle(50);
				pnlCenter.repaint();
			}
		});

		pnlSouth.add(btnCircle);
		btnCompositeShape.setToolTipText("Click to draw a composite shape.");
		btnCompositeShape.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				shape = new CompositeShape(new AShape[] {
						new Triangle(new int[] { 0, 100, 50 }, new int[] { 0, 0, 100 }), new Circle(50) });
				pnlCenter.repaint();
			}
		});

		pnlSouth.add(btnCompositeShape);
	}

}
